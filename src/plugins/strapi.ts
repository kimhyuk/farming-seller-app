import Strapi from "strapi-sdk-javascript";
import Vue from "vue";

Vue.use((Vue, options) => {
  const strapi = new Strapi(<string>"http://192.168.35.44:1337");

  Vue.prototype.$strapi = strapi;
  Vue.prototype.$apiUrl = "http://192.168.35.44:1337";
});
