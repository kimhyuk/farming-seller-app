import Vue from "vue";
const eventBus = new Vue();

Vue.use((Vue, options) => {
  Vue.prototype.$eventBus = eventBus;
});
