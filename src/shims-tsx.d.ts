import Vue, { VNode } from "vue";
import { EthereumProvider, provider, HttpProvider } from "web3-providers/types";
import Web3 from "web3/types";

declare global {
  namespace JSX {
    // tslint:disable no-empty-interface
    interface Element extends VNode {}
    // tslint:disable no-empty-interface
    interface ElementClass extends Vue {}
    interface IntrinsicElements {
      [elem: string]: any;
    }
  }
}
