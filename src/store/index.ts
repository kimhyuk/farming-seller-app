import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";
import modules from "./modules";
import { IRootState } from "./types";

Vue.use(Vuex);

const store: StoreOptions<IRootState> = {
  state: {
    version: "1.0.0"
  },
  modules
};

export default new Vuex.Store<IRootState>(store);
