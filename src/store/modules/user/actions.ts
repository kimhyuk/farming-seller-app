import { ActionTree } from "vuex";
import { ACTIONS, MUTATIONS } from "./constants";
import { IUserState } from "./types";
import { IRootState } from "@/store/types";
import jwtDecode from "jwt-decode";
import isElectron from "is-electron";

const electronStorage = isElectron()
  ? require("electron-json-storage")
  : undefined;

const actions: ActionTree<IUserState, IRootState> = {
  async [ACTIONS.SET_TOKEN]({ commit, dispatch }, token) {
    return new Promise((resolve, reject) => {
      if (electronStorage) {
        electronStorage.set("token", token, (err: any) => {
          if (err) throw err;

          return resolve(true);
        });
      } else {
        localStorage.setItem("token", token);
        dispatch(ACTIONS.SET_USER, token);

        return resolve(true);
      }
    });
  },
  async [ACTIONS.DELETE_TOKEN]({ commit, dispatch }) {
    return new Promise((resolve, reject) => {
      if (electronStorage) {
        electronStorage.remove("token", (err: any) => {
          if (err) throw err;

          return resolve(true);
        });
      } else {
        localStorage.removeItem("token");
        return resolve(true);
      }
    });
  },
  async [ACTIONS.GET_TOKEN]({ commit, dispatch }) {
    return new Promise((resolve, reject) => {
      if (electronStorage) {
        electronStorage.get("token", (err: any, data: any) => {
          resolve(data);
        });
      } else {
        const token = <string>localStorage.getItem("token");
        dispatch(ACTIONS.SET_USER, token);

        return resolve(token);
      }
    });
  },

  async [ACTIONS.SET_USER]({ commit, dispatch }, token) {
    var decoded = jwtDecode(token);
    commit(MUTATIONS.SET_USER, decoded);
  }
};

export default actions;
