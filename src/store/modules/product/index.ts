import { Module } from "vuex";
import { IProductState } from "./types";
import { IRootState } from "@/store/types";
//import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";
import state from "./state";
import constants from "./constants";

const product: Module<IProductState, IRootState> = {
  namespaced: true,
  state,
  actions,
  mutations
  //getters
};

export { constants, IProductState };
export default product;
