import ethereum from "./ethereum";
import dashboard from "./dashboard";
import user from "./user";
import product from "./product";

export default {
  ethereum,
  dashboard,
  user,
  product
};
