import { MUTATIONS } from "./constants";
import { MutationTree } from "vuex";
import { IDashboardState } from "./types";
// mutations
const mutations: MutationTree<IDashboardState> = {
  [MUTATIONS.DRAWER_VISIBLE_CHANGE](state) {
    state.drawerVisible = !state.drawerVisible;
  },
  [MUTATIONS.SET_ON_SNACKBAR](state, options) {
    state.snackbar.visible = options.visible;
    state.snackbar.message = options.message;
  }
};

export default mutations;
