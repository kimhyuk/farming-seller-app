import { ACTIONS, MUTATIONS } from "./constants";
import { ActionTree } from "vuex";
import { IRootState } from "@/store/types";
import { IEthereuemState } from "./types";
import { TransactionReceipt } from "web3-core";
import isElectron from "is-electron";
import { TYPE, STATEMUTABILITY } from "@/util/AbiReader";

const electronStorage = isElectron()
  ? require("electron-json-storage")
  : undefined;

const actions: ActionTree<IEthereuemState, IRootState> = {
  [ACTIONS.CLEAR]({ commit }) {
    commit(MUTATIONS.CLEAR);
  },
  [ACTIONS.INIT]({ commit }, payload) {
    commit(MUTATIONS.INIT, payload);
  },
  [ACTIONS.SET_WEB3]({ commit }, payload) {
    commit(MUTATIONS.SET_WEB3, payload);
  },
  [ACTIONS.SET_STATUS]({ commit }, payload) {
    commit(MUTATIONS.SET_STATUS, payload);
  },
  [ACTIONS.SET_CONTRACT]({ commit }, payload) {
    commit(MUTATIONS.SET_CONTRACT, payload);
  },
  [ACTIONS.SET_IS_CONTRACT]({ commit }, payload) {
    commit(MUTATIONS.SET_IS_CONTRACT, payload);
  },

  [ACTIONS.SET_ABI_READER]({ commit }, payload) {
    commit(MUTATIONS.SET_ABI_READER, payload);
  },

  [ACTIONS.SET_CONTRACT_NAME]({ commit }, payload) {
    commit(MUTATIONS.SET_CONTRACT_NAME, payload);
  },
  async [ACTIONS.SET_PRIVATE_KEY](_, privateKey) {
    if (electronStorage) {
      electronStorage.set("privateKey", privateKey, (err: any) => {
        if (err) throw err;

        return Promise.resolve(true);
      });
    } else {
      localStorage.setItem("privateKey", privateKey);
      return Promise.resolve(true);
    }
  },
  async [ACTIONS.DELETE_PRIVATE_KEY](_) {
    if (electronStorage) {
      electronStorage.remove("privateKey", (err: any) => {
        if (err) throw err;

        return Promise.resolve(true);
      });
    } else {
      localStorage.removeItem("privateKey");
      return Promise.resolve(true);
    }
  },
  async [ACTIONS.GET_PRIVATE_KEY](_) {
    if (electronStorage) {
      return new Promise((resolve, reject) => {
        electronStorage.get("privateKey", (err: any, data: any) => {
          if (err) throw err;

          return resolve(typeof data === "object" ? "" : data);
        });
      });
    } else {
      const privateKey = localStorage.getItem("privateKey");
      return Promise.resolve(privateKey);
    }
  },

  async [ACTIONS.REFRESH_BALANCE]({ state, commit }) {
    try {
      const { web3, contract, defaultAccount } = state;

      let decimal = state.decimals;

      let balance: string | number = 0;

      if (!state.isContract) {
        if (!web3) throw new Error("web3 is undefined");
        if (!defaultAccount) throw new Error("No Address");

        balance = await web3.eth.getBalance(defaultAccount);
        decimal = 18;
      } else {
        if (!contract) throw new Error("contract is undefined");

        balance = await contract.methods.balanceOf(state.defaultAccount).call();
        decimal = await contract.methods.decimals().call();
      }
      commit(MUTATIONS.SET_BALANCE, balance);
      commit(MUTATIONS.SET_DACIMALS, decimal);
      return Promise.resolve();
    } catch (err) {
      return Promise.reject(err);
    }
  },

  async [ACTIONS.SEND_TRANSACTION]({ state, commit }, { name, inputs }) {
    try {
      const { web3, contract, abiReader, defaultAccount } = state;

      if (!web3) throw new Error("web3 is undefined");
      if (!abiReader) throw new Error("abiReader is undefined");

      const abi = abiReader.find(name);

      if (!abi) throw new Error(`${name} is not function`);
      if (abi.inputs.length !== inputs.length)
        throw new Error("입력값이 부족합니다.");
      if (!contract) throw new Error("contract is undefined");

      if (abi.stateMutability === STATEMUTABILITY.view) {
        return contract.methods[name].apply(null, inputs).call();
      } else {
        return new Promise((resolve, reject) => {
          contract.methods[name].apply(null, inputs).send(
            {
              from: defaultAccount
            },
            (error: any, transactionHash: any) => {
              if (error) return reject(error);
              return resolve({ transactionHash });
            }
          );
        });
      }
    } catch (err) {
      throw err;
    }
  },

  [ACTIONS.ADD_TRANSACTION]({ dispatch, commit, state }, { hash, status }) {
    commit(MUTATIONS.ADD_TRANSACTION, { hash, status });
  },
  async [ACTIONS.SEND_COIN]({ dispatch, commit, state }, payload) {
    try {
      const { web3, defaultAccount, contract, isContract } = state;
      let decimal = state.decimals;

      if (isContract) {
        if (!contract) throw new Error("contract is undefined");
        const { transfer, deciamls } = contract.methods;

        if (!decimal) {
          decimal = await deciamls().call();
          commit(MUTATIONS.SET_DACIMALS, decimal);
        }

        const balance = Number(payload.value) * Math.pow(10, Number(decimal));

        return new Promise((resolve, reject) => {
          transfer(payload.to, String(balance)).send(
            {
              from: defaultAccount
            },
            (error: any, hash: string) => {
              if (error) return reject(error);

              dispatch(ACTIONS.ADD_TRANSACTION, { hash });
              resolve(hash);
            }
          );
        });
      } else {
        if (!web3) throw new Error("web3 is undefined");

        const gasPrice = await web3.eth.getGasPrice();

        var gasLimit = await web3.eth.estimateGas({
          to: payload.to,
          from: defaultAccount,
          value: web3.utils.toWei(String(payload.value), "ether")
        });

        const hash = await web3.eth.sendTransaction({
          from: defaultAccount,
          to: payload.to,
          value: web3.utils.toWei(String(payload.value), "ether"),
          gasPrice: payload.gasPrice || gasPrice,
          gas: payload.gas || gasLimit
        });

        dispatch(ACTIONS.ADD_TRANSACTION, { hash });

        return Promise.resolve(hash);
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }
};

export default actions;
