import Send from "@/components/dashboard/container/transaction/Send.vue";
import Products from "@components/dashboard/container/Products.vue";
import TradeLogs from "@/components/dashboard/container/TradeLogs.vue";
import Trade from "@/components/dashboard/container/Trade.vue";
import Dashboard from "@/views/Dashboard.vue";

export default {
  path: "/dashboard",
  name: "dashboard",
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  component: Dashboard,
  children: [
    // UserHome will be rendered inside User's <router-view>
    // when /user/:id is matched
    { path: "send", name: "send", component: Send },
    { path: "products", name: "product", component: Products },
    { path: "trade", name: "blacklist", component: Trade },
    { path: "tradelogs", name: "tradelogs", component: TradeLogs },
    { path: "*", redirect: "send" }
  ]
};
