import Vue from "vue";
import Router from "vue-router";
import Login from "@/views/Login.vue";
import Logout from "@/views/Logout.vue";
import dashboard from "./dashboard";
import store from "@store/index";
import { constants } from "@store/modules/ethereum";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "login",
      component: Login
    },
    {
      path: "/logout",
      name: "logout",
      component: Logout
    },
    dashboard,
    {
      path: "*",
      redirect: "/"
    }
  ]
});
