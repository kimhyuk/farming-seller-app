import Web3 from "web3";

type getWeiPrams = {
  type: string;
  privateKey?: string;
};

async function getApp(web3: Web3) {
  const [isListening, networkId, coinbase] = await Promise.all([
    web3.eth.net.isListening(),
    web3.eth.net.getId(),
    web3.eth.getCoinbase()
  ]);

  //let address = web3.eth.defaultAccount;
  const app = {
    web3,
    isListening,
    networkId,
    coinbase,
    defaultAccount: web3.eth.defaultAccount
  };
  return Promise.resolve(app);
}

async function loadMetamask() {
  let web3 = window.web3;

  if (typeof web3 === "undefined") {
    throw new Error("메타마스크 설치x");
  }
  web3 = new Web3(window.ethereum);
  await window.ethereum.enable();

  return getApp(web3);
}

async function loadPrivateKeyWallet(privateKey: string) {
  let web3 = new Web3(
    new Web3.providers.HttpProvider("http://192.168.35.44:8545")
  );

  const account = web3.eth.accounts.privateKeyToAccount("0x" + privateKey);
  web3.eth.accounts.wallet.add(account);
  web3.eth.defaultAccount = account.address;

  return getApp(web3);
}

export default async (params: getWeiPrams) => {
  const { type, privateKey } = params;
  try {
    if (type.toUpperCase() == "METAMASK") {
      return loadMetamask();
    } else {
      if (!privateKey) throw new Error("No PrivateKey");
      return loadPrivateKeyWallet(privateKey);
    }
  } catch (err) {
    throw err;
  }
};
