module.exports = {
	baseUrl: `${process.env.BASE_URL || "http://wepscoin.io"}`,
	apiUrl: `${process.env.API_URL || "http://api.wepscoin.io"}`
};
